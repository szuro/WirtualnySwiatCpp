/* 
 * File:   WilczeJagody.h
 * Author: robak
 *
 * Created on 19 października 2015, 22:40
 */

#include "../Roslina.h"

#ifndef WILCZEJAGODY_H
#define	WILCZEJAGODY_H

namespace roslina {

class WilczeJagody: public Roslina {
    
    public:
        WilczeJagody(int x, int y);
        ~WilczeJagody();
        void kolizja(organizm::Organizm* oponent);
        void rozmnazanie(int newX, int newY);

};
}

#endif	/* WILCZEJAGODY_H */

