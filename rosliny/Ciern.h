/* 
 * File:   Ciern.h
 * Author: robak
 *
 * Created on 19 października 2015, 22:40
 */

#include "../Roslina.h"

#ifndef CIERN_H
#define	CIERN_H

namespace roslina {

class Ciern: public Roslina {
    
    public:
        Ciern(int x, int y);
        ~Ciern();
        void akcja();
        void rozmnazanie(int newX, int newY);
    
};
}

#endif	/* CIERN_H */

