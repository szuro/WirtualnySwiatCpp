/* 
 * File:   Trawa.h
 * Author: robak
 *
 * Created on 19 października 2015, 22:30
 */

#include "../Roslina.h"

#ifndef TRAWA_H
#define	TRAWA_H

namespace roslina {

class Trawa: public Roslina {
    
    public:
        Trawa(int x, int y);
        ~Trawa();
        void rozmnazanie(int newX, int newY);
};
}

#endif	/* TRAWA_H */

