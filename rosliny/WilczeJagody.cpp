#include "WilczeJagody.h"
#include "../Swiat.h"

namespace roslina {

WilczeJagody::WilczeJagody(int x, int y) {
    this->x = x;
    this->y = y;
    symbol = 'j';
}

WilczeJagody::~WilczeJagody() {
    world->logger.push_back("Wilcze jagody zjedzone!");
}


void WilczeJagody::rozmnazanie(int newX, int newY) {
    
    naMapie[newX][newY] = new WilczeJagody(newX, newY);
    naMapie[newX][newY]->setSwiat(*world);
    world->organizmy.push_back(naMapie[newX][newY]);
    world->logger.push_back("Narodziny Wilczych jagód");

}

void WilczeJagody::kolizja(organizm::Organizm* oponent) {
    //stunuje przy smierci
    int tmp_sila = oponent->getSila();
    
    if (sila > tmp_sila) {
        oponent->setAlive(false);
    }
    else if (sila < tmp_sila) {
        setAlive(false);
        
    }
    
    if (!alive) {
        oponent->setAlive(false);
    }
    
}

}