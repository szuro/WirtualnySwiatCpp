/* 
 * File:   Wilk.h
 * Author: robak
 *
 * Created on 19 października 2015, 22:27
 */

#include "../Zwierze.h"

#ifndef WILK_H
#define	WILK_H

namespace zwierze {
//class Swiat;

class Wilk: public Zwierze {

    public:
        Wilk(int x, int y);
        ~Wilk();
        void rozmnazanie(organizm::Organizm* partner);

};
}

#endif	/* WILK_H */

