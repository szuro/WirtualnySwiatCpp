/* 
 * File:   Jez.h
 * Author: robak
 *
 * Created on 19 października 2015, 22:37
 */

#include "../Zwierze.h"

#ifndef JEZ_H
#define	JEZ_H

namespace zwierze {

class Jez: public Zwierze {
    
    public:
        Jez(int x, int y);
        ~Jez();
        void kolizja(organizm::Organizm* oponent);
        void rozmnazanie(organizm::Organizm* partner);

        
};
}

#endif	/* JEZ_H */

