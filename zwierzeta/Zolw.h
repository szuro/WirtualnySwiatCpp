/* 
 * File:   Zolw.h
 * Author: robak
 *
 * Created on 19 października 2015, 22:37
 */

#include "../Zwierze.h"

#ifndef ZOLW_H
#define	ZOLW_H

namespace zwierze {

class Zolw: public Zwierze {
    

    public:
        Zolw(int x, int y);
        ~Zolw();
        void rozmnazanie(organizm::Organizm* partner);
        void kolizja(organizm::Organizm* oponent);
        void akcja();

        
};
}

#endif	/* ZOLW_H */

