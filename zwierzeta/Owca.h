/* 
 * File:   Owca.h
 * Author: robak
 *
 * Created on 19 października 2015, 22:26
 */

#include "../Zwierze.h"

#ifndef OWCA_H
#define	OWCA_H

namespace zwierze {

class Owca: public Zwierze {
    
    public:
        Owca(int x, int y);
        ~Owca();
        void rozmnazanie(organizm::Organizm* partner);
};
}

#endif	/* OWCA_H */

