#include "Zolw.h"
#include "../Swiat.h"

namespace zwierze {

Zolw::Zolw(int x, int y) {
    this->x = x;
    this->y = y;
    inicjatywa = 1;
    sila = 1;
    symbol = 'Z';
    //std::cout << "Narodziny Zolwia!" << std::endl;
}

Zolw::~Zolw() {
    world->logger.push_back("Śółw padł!");
}

void Zolw::akcja() {

    if (ogluszenie == 0) {
    
        int wylosowaneCzolganie = rand() % 100;

        if (wylosowaneCzolganie < 25) {
            
            int kierunek = rand() % 4;
            const int wymiar = 20;

            int tmp_x = x;
            int tmp_y = y;

            if ((kierunek == 0) && (y > 0)) {
                //ruch w gore
                tmp_y -= 1;
            }
            else if ((kierunek == 1) && (x < wymiar - 1)) {
                //ruch w prawo
                tmp_x += 1;
            }
            else if ((kierunek == 2) && (y < wymiar - 1)) {
                //ruch w dol
                tmp_y += 1;
            }
            else if ((kierunek == 3) && (x > 0)) {
                //ruch w lewo
                tmp_x -= 1;
            }

            if (zolwniety) {
                zolwniety = false;
            }

            if (naMapie[tmp_x][tmp_y] == NULL) {
                naMapie[tmp_x][tmp_y] = naMapie[x][y];
                naMapie[x][y] = NULL;
                x = tmp_x;
                y = tmp_y;
            }
            else if (naMapie[tmp_x][tmp_y]->getSymbol() == symbol &&
                     naMapie[tmp_x][tmp_y] != &*this) {
                rozmnazanie(naMapie[tmp_x][tmp_y]); //sprawdz to
            }
            else if (naMapie[tmp_x][tmp_y] != &*this) {
                naMapie[tmp_x][tmp_y]->kolizja(&*this);
                kolizja(naMapie[tmp_x][tmp_y]);
                if (isAlive() && !zolwniety) {
                    naMapie[tmp_x][tmp_y] = naMapie[x][y];
                    naMapie[x][y] = NULL;
                    x = tmp_x;
                    y = tmp_y;
                }
                else if (!isAlive()) {
                    naMapie[x][y] = NULL;
                }
            }
        }
    }
    else {
        ogluszenie--;
    }
    if (zolwniety) {
        zolwniety = false;
    }
}

void Zolw::kolizja(organizm::Organizm* oponent) {
    //odpycha napastnikow o sile <5 na poprzednie pole
    int tmp_sila = oponent->getSila();

    if (sila > tmp_sila) {
        oponent->setAlive(false);
    }
    else if (tmp_sila < 5) {
        oponent->setZolwniety(true);
        zolwniety = true;
    }
    else if (tmp_sila > sila) {
        setAlive(false);
    }
//    else if (sila == tmpalive) {
//        oponent->setAlive(false);
//    }
}

void Zolw::rozmnazanie(organizm::Organizm* partner) {

    int tx = partner->getX();
    int ty = partner->getY();
    int j = 0;
    bool miejsceWgospodzie = false;
    
    int nx[] = {    x,     x, x + 1, x - 1,     tx,     tx, tx + 1, tx - 1};
    int ny[] = {y + 1, y - 1,     y,     y, ty + 1, ty - 1,     ty,     ty};

    for ( j; j < 8; j++ ) {
        if (nx[j] >= 0 && nx[j] < 20 &&
            ny[j] >= 0 && ny[j] < 20 &&
            naMapie[nx[j]][ny[j]] == NULL) {
            miejsceWgospodzie = true;
            break;
        }
    }
    
    if (miejsceWgospodzie) {
        naMapie[nx[j]][ny[j]] = new Zolw(nx[j], ny[j]);
        naMapie[nx[j]][ny[j]]->setSwiat(*world);
        world->organizmy.push_back(naMapie[nx[j]][ny[j]]);
        world->logger.push_back("Narodziny Żółwia");
    }
}

}