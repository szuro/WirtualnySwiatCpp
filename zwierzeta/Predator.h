/* 
 * File:   Predator.h
 * Author: robak
 *
 * Created on 19 października 2015, 22:27
 */

#include "../Zwierze.h"

#ifndef PREDATOR_H
#define	PREDATOR_H

namespace zwierze {

class Predator: public Zwierze {
    

    public:
        Predator(int x, int y);
        ~Predator();
        virtual void rozmnazanie(organizm::Organizm* partner);

    
};
}

#endif	/* PREDATOR_H */

