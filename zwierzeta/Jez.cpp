#include "Jez.h"
#include "../Swiat.h"

namespace zwierze {

Jez::Jez(int x, int y) {
    this->x = x;
    this->y = y;
    inicjatywa = 3;
    sila = 2;
    symbol = 'J';
    //std::cout << "Narodziny Jeza!" << std::endl;
}

Jez::~Jez() {
    world->logger.push_back("Jeż eksplodował!");
}

void Jez::kolizja(organizm::Organizm* oponent) {
    //stunuje przy smierci
    int tmp_sila = oponent->getSila();
    
    if (sila < 5 && zolwniety) {
        //nic =D
    }
    else if (sila > tmp_sila) {
        oponent->setAlive(false);
    }
    else if (sila < tmp_sila) {
        setAlive(false);
        
    }
    else if (alive) {
        setAlive(false);
    }
    
    if (!alive) {
        oponent->setOgluszenie();
    }
    
}

void Jez::rozmnazanie(organizm::Organizm* partner) {
    
    int tx = partner->getX();
    int ty = partner->getY();
    int j = 0;
    bool miejsceWgospodzie = false;
    
    int nx[] = {    x,     x, x + 1, x - 1,     tx,     tx, tx + 1, tx - 1};
    int ny[] = {y + 1, y - 1,     y,     y, ty + 1, ty - 1,     ty,     ty};

    for ( j; j < 8; j++ ) {
        if (nx[j] >= 0 && nx[j] < 20 &&
            ny[j] >= 0 && ny[j] < 20 &&
            naMapie[nx[j]][ny[j]] == NULL) {
            miejsceWgospodzie = true;
            break;
        }
    }
    
    if (miejsceWgospodzie) {
        naMapie[nx[j]][ny[j]] = new Jez(nx[j], ny[j]);
        naMapie[nx[j]][ny[j]]->setSwiat(*world);
        world->organizmy.push_back(naMapie[nx[j]][ny[j]]);
        world->logger.push_back("Narodziny Jeża");
    }
}

}