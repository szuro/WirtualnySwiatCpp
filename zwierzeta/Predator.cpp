#include "Predator.h"
#include "../Swiat.h"

namespace zwierze {

Predator::Predator(int x, int y) {
    this->x = x;
    this->y = y;
    inicjatywa = 50;
    sila = 40;
    symbol = 'P';
}

Predator::~Predator() {
    world->logger.push_back("Predator nie przegrywa!! nein! :<");
}

void Predator::rozmnazanie(organizm::Organizm* partner) {
 
    for (int i = 0; i < 2; i++) {

        int tx = partner->getX();
        int ty = partner->getY();
        int j = 0;
        bool miejsceWgospodzie = false;

        int nx[] = {    x,     x, x + 1, x - 1,     tx,     tx, tx + 1, tx - 1};
        int ny[] = {y + 1, y - 1,     y,     y, ty + 1, ty - 1,     ty,     ty};

        for ( j; j < 8; j++ ) {
            if (nx[j] >= 0 && nx[j] < 20 &&
                ny[j] >= 0 && ny[j] < 20 &&
                naMapie[nx[j]][ny[j]] == NULL) {
                miejsceWgospodzie = true;
                break;
            }
        }

        if (miejsceWgospodzie) {
            naMapie[nx[j]][ny[j]] = new Predator(nx[j], ny[j]);
            naMapie[nx[j]][ny[j]]->setSwiat(*world);
            world->organizmy.push_back(naMapie[nx[j]][ny[j]]);
            world->logger.push_back("Narodziny Predatora - zwiastuna śmierci!");
        }
    }
}

}