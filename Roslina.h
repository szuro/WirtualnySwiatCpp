/* 
 * File:   Roslina.h
 * Author: robak
 *
 * Created on 19 października 2015, 23:38
 */

#include "Organizm.h"



#ifndef ROSLINA_H
#define	ROSLINA_H

namespace roslina {

class Roslina: public organizm::Organizm {

    public:
        Roslina();
        virtual ~Roslina();
        virtual void kolizja(organizm::Organizm* oponent);
        virtual void rozmnazanie(organizm::Organizm* partner);
        virtual void rozmnazanie(int newX, int newY)=0;
        virtual void akcja();
        //Roslina(int x, int y);

          
};
}

#endif	/* ROSLINA_H */

