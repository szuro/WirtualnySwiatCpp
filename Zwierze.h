/* 
 * File:   Zwierze.h
 * Author: robak
 *
 * Created on 19 października 2015, 23:38
 */
#include "Organizm.h"

#ifndef ZWIERZE_H
#define	ZWIERZE_H

namespace zwierze {

class Zwierze: public organizm::Organizm {
    
    protected:
        virtual ~Zwierze();
        virtual void rozmnazanie(organizm::Organizm* partner)=0;
        virtual void rozmnazanie(int newX, int newY);
        virtual void kolizja(organizm::Organizm* oponent);
        virtual void akcja();
        //Zwierze(int x, int y);

    
};
}

#endif	/* ZWIERZE_H */

