/* 
 * File:   main.cpp
 * Author: robak
 *
 * Created on 19 października 2015, 22:09
 */

#include <fstream>
#include <ctime>
#include <cstring>
#include <assert.h>
//swiat
#include "Organizm.h"
#include "Swiat.h"
//rosliny
#include "rosliny/Ciern.h"
#include "rosliny/Trawa.h"
#include "rosliny/WilczeJagody.h"
////zwierzeta
#include "zwierzeta/Jez.h"
#include "zwierzeta/Owca.h"
#include "zwierzeta/Predator.h"
#include "zwierzeta/Wilk.h"
#include "zwierzeta/Zolw.h"

//using namespace std;
using namespace swiat;
/*
 *  #include <ifstream>
 *  ifsteram plik("plik.txt");
 */

void inputParser(Swiat & zaWarudo, int ilosc, char ** args);

int main(int argc, char** argv) {

    srand(time(0)/*trzeba dac dobry seed*/);

    Swiat zaWarudo;

    inputParser(zaWarudo, argc, argv);

    //petelka swiata
    zaWarudo.rysujSwiat();

    std::string control;
    while (true/*(control = std::cin.get()) != 27*/) {
        std::cout << "Wpisz polecenie: ";
        std::cin >> control;
        if (control == "save") {
            std::cout << "Podaj nazwe pliku: ";
            std::string filename;
            std::cin >> filename;
            std::ofstream plik(filename);
            //plik.open(filename);
            try {
                if (plik.is_open()) {
                    plik << zaWarudo.zachowajSwiat();
                }
                else {
                    throw 45;
                }
            } catch (int e) {
                std::cout << "Błąd " << e;
                std::cout << " - nieprawidłowy plik" << std::endl;
                std::cout << "Używam domyślnego" << std::endl;
                std::ofstream plik("save.txt");
                plik << zaWarudo.zachowajSwiat();
            }
            plik.close();
            //zaWarudo.~Swiat();
            //return 0;
            //exit(EXIT_SUCCESS);
        }
        else if (control == "exit") {
            break;
        }
        zaWarudo.wykonajTure();
        zaWarudo.rysujSwiat();
    }

    return 0;
}

void inputParser(Swiat & zaWarudo, int argc, char ** argv) {

    std::vector<std::string> organizmy;

    for (int i = 1; i < argc; i++) {

        if (strcmp(argv[i], "-f") == 0 &&
                i + 1 < argc) {

            std::string linia;
            std::ifstream plik(argv[i + 1]);
            if (plik.is_open()) {
                while (plik >> linia) {
                    organizmy.push_back(linia);
                }
                plik.close();
                break;
            }
        }
        else if (strcmp(argv[i], "-f") == 0 &&
                i == argc -1) {
            std::cout << "Brak podanego pliku" << std::endl;
            break;
        }
        else {
            std::string str(argv[i]);
            organizmy.push_back(str);
        }

    }

    if (organizmy.size() == 0) {
        std::cout << "Brak organizmów" << std::endl;
        exit(EXIT_FAILURE);
    }

    const char* znak;
    for (int i = 0; i < organizmy.size(); i++) {

        znak = organizmy[i].c_str();
        char zwierzak = 0;
        int x = -1;
        int y = -1;
        int w = 0;
        int s = 0;

        while (*znak != 0) {
            switch (*znak) {
                case 'W': zwierzak = 'W';
                    break;
                case 'J': zwierzak = 'J';
                    break;
                case 'Z': zwierzak = 'Z';
                    break;
                case 'P': zwierzak = 'P';
                    break;
                case 'O': zwierzak = 'O';
                    break;
                case 'c': zwierzak = 'c';
                    break;
                case 'j': zwierzak = 'j';
                    break;
                case 't': zwierzak = 't';
                    break;
                case 'x': x = atoi(znak + 1);
                    break;
                case 'y': y = atoi(znak + 1);
                    break;
                case 'w': w = atoi(znak + 1);
                    break;
                case 's': s = atoi(znak + 1);
                    break;
                default:
                    try {
                        if (!isdigit(*znak)) {
                            throw *znak;
                        }
                    } catch (char e) {
                        std::cout << "Niepoprawny znak: " << e << std::endl;
                        std::cout << "Dopuszczalne znaki:" << std::endl;
                        std::cout << "W - wilk; O - Owca; Z - Żółw" << std::endl;
                        std::cout << "P - predator; J - Jeż" << std::endl;
                        std::cout << "j - wilcze jagody; c - cierń; t - trawa" << std::endl;
                        std::cout << "x - położenie w osi X; y - położenie w osi Y" << std::endl;
                        std::cout << "w - wiek organizmu; s - ile tur organizm będzie ogłuszony" << std::endl;
                        std::cout << "Przykładowa konstrukcja: Px2y0w5s0 Wx13y7" << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    ;
                    break;
            }
            znak++;
        }
        assert(x >= 0);
        assert(y >= 0);
        assert(x < 20);
        assert(y < 20);
        assert(zwierzak != 0);
        //sprawdzanie, czy mapaOrganizmow[x][y] == NULL
        if (zaWarudo.mapaOrganizmow[x][y] == NULL) {
            switch (zwierzak) {
                case 'W':
                    zaWarudo.mapaOrganizmow[x][y] = new zwierze::Wilk(x, y);
                    break;
                case 'J':
                    zaWarudo.mapaOrganizmow[x][y] = new zwierze::Jez(x, y);
                    break;
                case 'Z':
                    zaWarudo.mapaOrganizmow[x][y] = new zwierze::Zolw(x, y);
                    break;
                case 'O':
                    zaWarudo.mapaOrganizmow[x][y] = new zwierze::Owca(x, y);
                    break;
                case 'P':
                    zaWarudo.mapaOrganizmow[x][y] = new zwierze::Predator(x, y);
                    break;
                case 'c':
                    zaWarudo.mapaOrganizmow[x][y] = new roslina::Ciern(x, y);
                    break;
                case 'j':
                    zaWarudo.mapaOrganizmow[x][y] = new roslina::WilczeJagody(x, y);
                    break;
                case 't':
                    zaWarudo.mapaOrganizmow[x][y] = new roslina::Trawa(x, y);
                    break;
                    //default: break;
            }
            //podstawowe parametry przy wczytywaniu
            zaWarudo.organizmy.push_back(zaWarudo.mapaOrganizmow[x][y]);
            zaWarudo.mapaOrganizmow[x][y]->setSwiat(zaWarudo);
            zaWarudo.mapaOrganizmow[x][y]->setWiek(w);
            zaWarudo.mapaOrganizmow[x][y]->setOgluszenie(s);
        }
        else {
            std::cout << "Pole zajęte" << std::endl;
        }
    }
}