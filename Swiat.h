/* 
 * File:   Swiat.h
 * Author: robak
 *
 * Created on 19 października 2015, 22:42
 */

#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <string>
#include "Organizm.h"

#ifndef SWIAT_H
#define	SWIAT_H

namespace swiat {
//class Organizm;

class Swiat {
    private:
        //Organizm* organizmy = new Organizm[20];
//        std::size_t size = 5;
        //public?
        static bool sortowanie(organizm::Organizm* i, organizm::Organizm* j);
        static bool sortowanieZywych(organizm::Organizm* i, organizm::Organizm* j);
    public:
        organizm::Organizm*** mapaOrganizmow;
        std::vector<organizm::Organizm*> organizmy;
        std::vector<std::string> logger;
        
        char** mapa;
        void wykonajTure();
        void rysujSwiat();
        void utworzSwiat();
        std::string zachowajSwiat();
        //void narodziny();
        Swiat();
        ~Swiat();
};
}
#endif	/* SWIAT_H */

